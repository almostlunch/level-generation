﻿using UnityEngine;
using System.Collections;

public class PlaceRooms : MonoBehaviour
{
    //GAMEOBJECTS:
    public GameObject room;
    public GameObject spawnRoom;
    public GameObject bossRoom;
    public GameObject corridor;
    
    //DUNGEON PARAMETERS:  
    public int minNoRooms, maxNoRooms;
    private Vector3 minDungeonSize = new Vector3 (0, 0, 0);
    public Vector3 maxDungeonSize;
    private Vector3 dungeonSize = new Vector3(0, 0, 0);

    //CORRIDORS:
    private Vector3 prePos, curPos;

    //CHECK VARIABLES:
    bool firstRoom = true;

    //VARIABLES FOR ROOM SIZE:
    private Vector3 RoomSize; //x and y dimentions of the room
    public Vector3 MaxRoomSize; //set maximum dimensions for each room
    public Vector3 MinRoomSize; //set min dimentsions for each room

    //VARIABLES FOR SPAWN ROOM SIZE:
    public Vector3 spawnRoomMinSize, spawnRoomMaxSize;
    private Vector3 spawnRoomSize;

    private float ranDis;
    void Awake()
    {
        //variables
        float previousX = 0;
        ranDis = Random.Range(MaxRoomSize.x, (MaxRoomSize.x + 30));
        //randomly choose number of rooms
        int noOfRooms = Random.Range(minNoRooms, maxNoRooms);
        //create room "noOfRooms" amount of times
        for (int i = 0; i <= noOfRooms; i++)
        {
            if (firstRoom == true) //place SPAWN room
            {
                genSpawnRoomSize();
                Instantiate(spawnRoom, dungeonSize, Quaternion.identity);               
                firstRoom = false;
            }
            else if (i == noOfRooms) //place BOSS room
            {
                previousX += 100;
                //dungeonSize.x = Random.Range(previousX, maxDungeonSize.x);
                dungeonSize.x = previousX;
                dungeonSize.y = Random.Range(minDungeonSize.y, maxDungeonSize.y);
                dungeonSize.z = Random.Range(minDungeonSize.z, maxDungeonSize.z);
                Instantiate(bossRoom, dungeonSize, Quaternion.identity);               
            }
            else //place ROOMS
            {
                prePos = dungeonSize;
                previousX += 45;
                //dungeonSize.x = Random.Range(previousX, maxDungeonSize.x);
                dungeonSize.x = previousX;
                dungeonSize.y = Random.Range(minDungeonSize.y, maxDungeonSize.y);
                dungeonSize.z = Random.Range(minDungeonSize.z, maxDungeonSize.z);
                previousX = dungeonSize.x;
                curPos = dungeonSize;               
                conRooms();
                genRoomSize();
                Instantiate(room, dungeonSize, Quaternion.identity);
                //conRooms();               
            }
         }
    }

    void conRooms()
    { 
        Vector3 corL = new Vector3(0, 0.1f, 1);
        corL.x = curPos.x - prePos.x;
        corridor.transform.localScale = corL;
        if (firstRoom == true)
        {
            prePos.x += spawnRoom.transform.localScale.x / 2;
        }
        else
        {
            prePos.x += room.transform.localScale.x / 2;
        }
        Instantiate(corridor, prePos, Quaternion.identity);
    }

    void genRoomSize()
    {
        //SET RANDOM ROOM DIMENTIONS
        RoomSize.x = Random.Range(MaxRoomSize.x, MinRoomSize.x);
        RoomSize.y = Random.Range(MaxRoomSize.y, MinRoomSize.y);
        RoomSize.z = Random.Range(MaxRoomSize.z, MinRoomSize.z);
        room.transform.localScale = RoomSize;
    }

    void genSpawnRoomSize()
    {
        spawnRoomSize.x = Random.Range(spawnRoomMinSize.x, spawnRoomMaxSize.x);
        spawnRoomSize.y = Random.Range(spawnRoomMinSize.y, spawnRoomMaxSize.y);
        spawnRoomSize.z = Random.Range(spawnRoomMinSize.z, spawnRoomMaxSize.z);
        spawnRoom.transform.localScale = spawnRoomSize;
    }
}
