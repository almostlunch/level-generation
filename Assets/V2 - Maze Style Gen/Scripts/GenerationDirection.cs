﻿using UnityEngine;

public enum GenerationDirection {
    //used to randomly change direction of level generation
    North,
    East,
    South,
    West	
}

public static class GenerationDirections
{
    public const int NoOf = 4; //4 possible direction *see (enum) GenerationDirection

    public static GenerationDirection RandomValue
    {
        get
        {
            return (GenerationDirection)Random.Range(0, NoOf);
        }
    }

    //used to ajust current coords based on random direction ^
    private static IntVector2[] vectors =
    {
        new IntVector2(0, 10),
        new IntVector2(10, 0),
        new IntVector2(0, -10),
        new IntVector2(-10, 0)
    };

    //used to convert direction into an int
    //uses this so that it behavies as if it were an instance method of generationDirection
    public static IntVector2 ToIntVector2(this GenerationDirection dir)
    {
        return vectors[(int)dir];
    }

    //reverse current direction
    private static GenerationDirection[] opposites =
    {
        GenerationDirection.South,
        GenerationDirection.West,
        GenerationDirection.North,
        GenerationDirection.East
    };

    public static GenerationDirection GetOpposite(this GenerationDirection direction)
    {
        return opposites[(int)direction];
    }
    //used to rotate walls to correct face of cell
    private static Quaternion[] rotations = {
        Quaternion.identity,
        Quaternion.Euler(0f, 90f, 0f),
        Quaternion.Euler(0f, 180f, 0f),
        Quaternion.Euler(0f, 270f, 0f)
    };

    public static Quaternion Rotate(this GenerationDirection direction)
    {
        return rotations[(int)direction];
    }

}
