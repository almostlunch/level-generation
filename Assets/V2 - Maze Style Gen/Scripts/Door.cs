﻿using UnityEngine;
using System.Collections;

public class Door : Passage {

    public Transform hinge;

    //prevent door from having all handles on right side
    private Door OpositeSideOfDoor {
        get
        {
            return otherCell.GetEdge(direction.GetOpposite()) as Door;
        }
    }

    public override void InitializeEdges(LevelCell primary, LevelCell other, GenerationDirection direction)
    {
        base.InitializeEdges(primary, other, direction);
        if (OpositeSideOfDoor != null)
        {
            hinge.localScale = new Vector3(-1f, 1f, 1f);
            Vector3 p = hinge.localPosition;
            p.x = -p.x;
            hinge.localPosition = p;
        }

        //set door frame to correct colour 
        for (int i = 0; i < transform.childCount; i++) {
            Transform child = transform.GetChild(i);
            if (child != hinge)
            {
                child.GetComponent<Renderer>().material = cell.room.settings.wallMat;
            }
        }
    }
}
