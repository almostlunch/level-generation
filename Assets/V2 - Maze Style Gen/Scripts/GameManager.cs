﻿using UnityEngine;
using System.Collections;

public class GameManager : MonoBehaviour {

    //Variables
    public GameWorld levelPrefab;
    private GameWorld levelInstance; 

	// Use this for initialization
	void Start () {
        Initialize();
	}
	
	// Update is called once per frame
	void Update () {
	    if (Input.GetKeyDown(KeyCode.Space))
        {
            Restart();
        }
	}

    private void Initialize()
    {
        levelInstance = Instantiate(levelPrefab);
        StartCoroutine(levelInstance.Generate());
    }

    private void Restart()
    {
        StopAllCoroutines();
        Destroy(levelInstance.gameObject);
        Initialize();
    }
}
