﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic; //used when creating list of cells to prevent gen from stopping

public class GameWorld : MonoBehaviour
{

    //VARIABLES:
    //public int sizeX, sizeZ; <- replaced with IntVector2
    public float cellGenerationDelay; //used to add a delay during generation to vusualise level generation
    public IntVector2 size; //used to set size of the dungeon (x, z)

    //prefabs etc,.
    public LevelCell cellPrefab; //cube with quad floor
    public LevelCell[,] cells; //array of cells
    public Passage passagePrefab; // empty cell
    public Wall[] wallPrefabs; //cell with wall on north face

    //boss room
    public LevelCell bossCellPrefab;
    bool firstCell = true;

    //door system
    public Door doorPrefab; //door prefab attatched in editor
    [Range(0f, 1f)]
    public float doorProb; //door probibility


    //coloured room system
    public RoomSettings[] roomSettings;

    public LevelCell GetCell(IntVector2 coordinates)
    {
        return cells[coordinates.x, coordinates.z];
    }

    public IEnumerator Generate()
    {
        WaitForSeconds genDelay = new WaitForSeconds(cellGenerationDelay);
        cells = new LevelCell[size.x, size.z];
        //list of active cells to prevent cell gen from halting upon bumping into each other
        List<LevelCell> activeCells = new List<LevelCell>();
        DoFirstGenStep(activeCells);
        while (activeCells.Count > 0)
        {
            yield return genDelay;
            DoNextGenStep(activeCells);
        }

        /*
        GENERATES size.x* size.y SIZED FLAT DUNGEON
        for (int x = 0; x < size.x; x += 1)
        {
            for (int z = 0; z < size.z; z += 1)
            {
                yield return genDelay;
                GenCell(new IntVector2(x, z));
            }
        } */
    }

    private LevelCell GenCell(IntVector2 coordinates)
    {
        if (firstCell)
        {
            LevelCell newCell = Instantiate(bossCellPrefab) as LevelCell;
            firstCell = false;
            cells[coordinates.x, coordinates.z] = newCell;
            newCell.coordinates = coordinates;
            newCell.name = "Level Cell " + coordinates.x + "," + coordinates.z;
            newCell.transform.parent = transform;
            newCell.transform.localPosition = new Vector3(coordinates.x - size.x * 0.5f + 0.5f, 0f, coordinates.z - size.z * 0.5f + 0.5f);
            return newCell;
        }
        else
        {
            LevelCell newCell = Instantiate(cellPrefab) as LevelCell;
            cells[coordinates.x, coordinates.z] = newCell;
            newCell.coordinates = coordinates;
            newCell.name = "Level Cell " + coordinates.x + "," + coordinates.z;
            newCell.transform.parent = transform;
            newCell.transform.localPosition = new Vector3(coordinates.x - size.x * 0.5f + 0.5f, 0f, coordinates.z - size.z * 0.5f + 0.5f);
            return newCell;
        }
    }

    //produces random x and z coordinates 
    public IntVector2 RandomCoords
    {
        get
        {
            return new IntVector2(Random.Range(0, size.x), Random.Range(0, size.z));
        }
    }

    //checks to see if coordinates are located within the dungeon (size)
    public bool ContainsCoords(IntVector2 coordinate)
    {
        return coordinate.x >= 0 && coordinate.x < size.x && coordinate.z >= 0 && coordinate.z < size.z;
    }

    //adds the first cell the the list of cells
    private void DoFirstGenStep(List<LevelCell> activeCells)
    {
        LevelCell newCell = GenCell(RandomCoords);
        newCell.Initialize(GenRoom(-1));
        activeCells.Add(newCell);
    }

    //used to create actual rooms!
    private void GenPassageInRoom(LevelCell cell, LevelCell otherCell, GenerationDirection direction)
    {
        Passage passage = Instantiate(passagePrefab) as Passage;
        passage.InitializeEdges(cell, otherCell, direction);
        passage = Instantiate(passagePrefab) as Passage;
        passage.InitializeEdges(otherCell, cell, direction.GetOpposite());
        //check wether its connecting different rooms
        if (cell.room != otherCell.room)
        {
            Room roomToAssimilate = otherCell.room;
            cell.room.Assimilate(roomToAssimilate);
            rooms.Remove(roomToAssimilate);
            Destroy(roomToAssimilate);
        }
    }

    //if generation cannot move one step from current cell, remove cell from list and try again
    private void DoNextGenStep(List<LevelCell> activeCells)
    {
        int curIndex = activeCells.Count - 1;
        LevelCell curCell = activeCells[curIndex];
        if (curCell.IsFullyInitialized)
        {
            activeCells.RemoveAt(curIndex);
            return;
        }
        GenerationDirection direction = curCell.RandomUnitilalizedDirection;
        IntVector2 coordinates = curCell.coordinates + direction.ToIntVector2();
        if (ContainsCoords(coordinates))
        {
            LevelCell neighbour = GetCell(coordinates);
            if (neighbour == null) //check if no cell next to current cell and creates passage if there isnt 
            {
                neighbour = GenCell(coordinates);
                GenPassage(curCell, neighbour, direction);
                activeCells.Add(neighbour);
            }else if (curCell.room.settingsIndx == neighbour.room.settingsIndx) //removes walls inside of rooms 
            {
                GenPassageInRoom(curCell, neighbour, direction);
            }
            else //creates wall if there is 
            {
                GenWall(curCell, neighbour, direction);             
            }
        }
        else
        {
            GenWall(curCell, null, direction);
        }
    }

    private void GenPassage (LevelCell cell, LevelCell otherCell, GenerationDirection direction)
    {
        Passage prefab = Random.value < doorProb ? doorPrefab : passagePrefab;
        Passage passage = Instantiate(prefab) as Passage;
        passage.InitializeEdges(cell, otherCell, direction);
        passage = Instantiate(prefab) as Passage;
        if (passage is Door)
        {
            otherCell.Initialize(GenRoom(cell.room.settingsIndx));
        } else
        {
            otherCell.Initialize(cell.room);
        }
        passage.InitializeEdges(otherCell, cell, direction.GetOpposite()); 
    }

    private void GenWall (LevelCell cell, LevelCell otherCell, GenerationDirection direction)
    {
        Wall wall = Instantiate(wallPrefabs[Random.Range(0, wallPrefabs.Length)]) as Wall;
        wall.InitializeEdges(cell, otherCell, direction);
        if (otherCell != null)
        {
            wall = Instantiate(wallPrefabs[Random.Range(0, wallPrefabs.Length)]) as Wall;
            wall.InitializeEdges(otherCell, cell, direction.GetOpposite());
        }
    }

    //used for room assignment
    private List<Room> rooms = new List<Room>();

    private Room GenRoom (int indexToExclude)
    {
        Room newRoom = ScriptableObject.CreateInstance<Room>();
        newRoom.settingsIndx = Random.Range(0, roomSettings.Length);
        if (newRoom.settingsIndx == indexToExclude)
        {
            newRoom.settingsIndx = (newRoom.settingsIndx + 1) % roomSettings.Length; //% -  computes the remainder of a division
        }
        newRoom.settings = roomSettings[newRoom.settingsIndx];
        rooms.Add(newRoom);
        return newRoom;
    }
}
