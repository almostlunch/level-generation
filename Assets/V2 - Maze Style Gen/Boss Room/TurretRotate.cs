﻿using UnityEngine;
using System.Collections;

public class TurretRotate : MonoBehaviour {

    
    public Transform target;
    public float rotSpeed;

    private Quaternion lookRot;
    private Vector3 direction;
    
	
	
	// Update is called once per frame
	void Update () {

        direction = (target.position - transform.position).normalized;

        lookRot = Quaternion.LookRotation(direction);

        transform.rotation = Quaternion.Slerp(transform.rotation, lookRot, Time.deltaTime * rotSpeed);


	}
}
