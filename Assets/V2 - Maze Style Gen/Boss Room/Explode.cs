﻿using UnityEngine;
using System.Collections;

public class Explode : MonoBehaviour {

    public GameObject Explosion;
	
    void OnTriggerEnter(Collider col)
    {
        if (col != transform.parent)
        {
            Instantiate(Explosion, transform.position, transform.rotation);
            Destroy(transform.parent.gameObject);
        }
    }

}
