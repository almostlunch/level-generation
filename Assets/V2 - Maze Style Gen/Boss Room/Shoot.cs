﻿using UnityEngine;
using System.Collections;

public class Shoot : MonoBehaviour {

    public Rigidbody rocket;
    public float force;
    private float timer;
    public float shootDelay;

    void Update()
    {
        timer += Time.deltaTime;
        if (timer > shootDelay)
        {
            shoot();
            timer = 0;
        }
    }

    void shoot()
    {
        Rigidbody shellInstance = Instantiate(rocket, transform.position, transform.rotation) as Rigidbody;
        shellInstance.velocity = transform.forward * force;
        //shellInstance.AddForce(new Vector3(force,0,0)); 
    }
}
